package Controller;

import Dao.ClientDao;
import Dao.OrderDaoClass;
import Dao.OrderDetailsDao;
import Dao.ProductDao;
import Model.Client;
import Model.Comanda;
import Model.OrderDetails;
import Model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class ControllerClass {

    private int nrp = 0;
    private int nrc = 0;
    private int nro = 0;
    private int nrf = 0;

    public void action(String[] com) {
        String[] command = com[0].split(" ");
        String[] words = new String[10];
        for (String comm : com) {
            words = split(comm, ", ");
        }
        if (command[0].equals("Insert") && command[1].equals("client")) {
            Client c = new Client(words[0], words[1], 0);
            ClientDao q = new ClientDao();
            q.insertClient(c);
        } else if (command[0].equals("Insert") && command[1].equals("product")) {
            Product p = new Product(words[0], Integer.parseInt(words[1]), Float.parseFloat(words[2]), 0);
            ProductDao q = new ProductDao();
            q.insertProduct(p);
        } else if (command[0].equals("Delete") && command[1].equals("client")) {
            deleteclient(words[0]);
        } else if (command[0].equals("Delete") && (command[1].equals("Product") || command[1].equals("product"))) {
            deleteproduct(words[0]);
        } else if (command[0].equals("Order")) {
            order(words);
        } else if (command[0].equals("Report") && command[1].equals("client")) {
            nrc++;
            reportClient(nrc);
        } else if (command[0].equals("Report") && command[1].equals("product")) {
            nrp++;
            reportProduct(nrp);
        } else if (command[0].equals("Report") && command[1].equals("order")) {
            nro++;
            reportOrder(nro);
        }
    }

    public void deleteproduct(String pk) {
        Product p = new Product(null, 0, 0, 0);
        ProductDao q = new ProductDao();
        q.deleteProduct(p, pk, "name_product");
    }

    public void deleteclient(String pk) {
        Client c = new Client(null, null, 0);
        ClientDao q = new ClientDao();
        q.deleteClient(c, pk, "name_client");
    }

    public void order(String[] words) {
        OrderDaoClass dao = new OrderDaoClass();
        OrderDetailsDao doa = new OrderDetailsDao();
        ProductDao pdao = new ProductDao();
        OrderDetails orderDetails = new OrderDetails(words[0], words[1], Integer.parseInt(words[2]), pdao.getprice(words[1]), 0);
        if (doa.insertOrderDetails(orderDetails) == 0) {
            nrf++;
            dao.insertOrder(new Comanda(orderDetails), nrf);
        } else {
            Document document = new Document();
            try {
                nrf++;
                PdfWriter.getInstance(document, new FileOutputStream("Factura" + nrf + ".pdf"));
                document.open();
                Font font = FontFactory.getFont(FontFactory.TIMES_BOLD, 25, BaseColor.BLACK);
                Font font2 = FontFactory.getFont(FontFactory.TIMES_BOLD, 15, BaseColor.RED);
                Paragraph p = new Paragraph("FACTURA", font);
                p.setSpacingAfter(20);
                p.setAlignment(1);
                Chunk c = new Chunk("Comanda indisponibila:Stoc insuficient", font2);
                document.add(p);
                document.add(c);
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            document.close();
        }
    }

    public void reportOrder(int nr) {
        OrderDetailsDao dao = new OrderDetailsDao();

        try {
            dao.generatePdf(nr);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public void reportProduct(int nr) {
        ProductDao dao = new ProductDao();

        try {
            dao.generatePdf(nr);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    public void reportClient(int nr) {
        ClientDao dao = new ClientDao();
        try {
            dao.generatePdf(nr);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    public String[] split(String string, String desp) {
        String[] newString = new String[100];
        newString = string.split(desp);
        return newString;
    }




    public void read(String file) {
        File f = new File(file);
        Scanner read = null;
        try {
            read = new Scanner(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String[] comanda = new String[15];
        String citit;
        String[] words = new String[15];
        while (read.hasNext()) {
            citit = read.nextLine();
            comanda = split(citit, ": ");
            action(comanda);

        }
    }

}
