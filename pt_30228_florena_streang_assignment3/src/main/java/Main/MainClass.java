package Main;

import Controller.ControllerClass;
import Dao.ClientDao;
import Dao.ProductDao;
import Model.Client;
import Model.Product;

public class MainClass {

    public static void main(String[] args) {
        ControllerClass c = new ControllerClass();
        c.read(args[1]);
    }
}
