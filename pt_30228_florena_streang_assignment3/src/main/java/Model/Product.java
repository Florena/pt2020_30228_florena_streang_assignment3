package Model;

public class Product {
    private String name_product;
    private int quantity;
    private double price;
    private int flag;

    public Product(String name_product, int quantity, double price,int flag) {
        this.name_product = name_product;
        this.quantity = quantity;
        this.price = price;
        this.flag=flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int update(int quantity) {
        this.quantity-=quantity;
        return this.quantity;
    }
}
