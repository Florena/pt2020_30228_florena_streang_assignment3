package Model;

public class Comanda {


        //public int id;
        public String name_client;
        public String name_product;
        public int quantity;
        public double total;

        public Comanda(OrderDetails orderDetails) {
            //this.id=id;
            this.name_client = orderDetails.getName_client();
            this.name_product = orderDetails.getName_product();
            this.quantity = orderDetails.getQuantity();
            this.total = orderDetails.getPrice() * orderDetails.getQuantity();
        }

//        public int getId() {
//            return id;
//        }
//
//        public void setId(int id) {
//            this.id = id;
//        }

        public String getName_client() {
            return name_client;
        }

        public void setName_client(String name_client) {
            this.name_client = name_client;
        }

        public String getName_product() {
            return name_product;
        }

        public void setName_product(String name_product) {
            this.name_product = name_product;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(double total) {
            this.total = total;
        }
    }



