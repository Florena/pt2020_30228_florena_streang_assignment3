package Model;

public class Client {
    private String name_client;
    private String address;
    private int flag;

    public Client(String name_client, String address,int flag) {
        this.name_client = name_client;
        this.address = address;
        this.flag=flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getName_client() {
        return name_client;
    }

    public void setName_client(String name_client) {
        this.name_client = name_client;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
