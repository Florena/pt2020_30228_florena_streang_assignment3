package Model;

public class OrderDetails {
    public String name_client;
    public  String name_product;
    public int quantity;
    public double price;
    public int flag;

    public OrderDetails(String name_client, String name_product, int quantity,double price,int flag) {
        this.name_client = name_client;
        this.name_product = name_product;
        this.quantity = quantity;
        this.price=price;
        this.flag=flag;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getName_client() {
        return name_client;
    }

    public void setName_client(String name_client) {
        this.name_client = name_client;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
