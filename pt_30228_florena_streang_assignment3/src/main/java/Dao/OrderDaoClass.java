package Dao;

import ConnectionDB.ConnectionFactory;
import Model.Comanda;
import Model.OrderDetails;
import Model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Stream;

public class OrderDaoClass extends GenericDao<Comanda> {

    public OrderDaoClass() {
        super();
    }

    public void insertOrder(Comanda order, int nr){
        if(insert(order)==0) {
            try {
                generatePdf(nr,order);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }

    }

    public void generatePdf(int nr,Comanda c) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("Factura"+nr+".pdf"));
        document.open();
        PdfPTable table = new PdfPTable(5);


        Font font = FontFactory.getFont(FontFactory.TIMES_BOLD, 25, BaseColor.BLACK);
        Paragraph p=new Paragraph("FACTURA",font);
        p.setSpacingAfter(20);
        p.setAlignment(1);
        addTableHeader(table);
        addRows(table,c);
        document.add(p);
        document.add(table);
        document.close();
    }

    private void addRows(PdfPTable table,Comanda comanda) {
        OrderDaoClass dao = new OrderDaoClass();
        int nr = 1;
            Stream.of(Integer.toString(nr),comanda.getName_client(),comanda.getName_product(), Integer.toString(comanda.getQuantity()),Double.toString(comanda.getTotal())).forEach(row -> {
                PdfPCell cell = new PdfPCell();
                cell.setBackgroundColor(BaseColor.PINK);
                cell.setBorderWidth(3);
                cell.setBorderColor(BaseColor.DARK_GRAY);
                cell.setPhrase(new Phrase(row));
                table.addCell(cell);
            });

    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("Nr.crt","Nume si Prenume","Produs", "Cantitate","Total").forEach(columnTitle -> {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.PINK);
            header.setBorderWidth(3);
            header.setBorderColor(BaseColor.DARK_GRAY);
            header.setPhrase(new Phrase(columnTitle));
            table.addCell(header);
        });

    }

}
