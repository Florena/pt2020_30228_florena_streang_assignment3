package Dao;

import ConnectionDB.ConnectionFactory;
import Model.Client;
import Model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ProductDao extends GenericDao<Product> {


    public ProductDao() {
        super();
    }

    public void insertProduct(Product p){
        insert(p);
    }

    public void deleteProduct(Product p, String pk, String column){
        delete(p,pk,column,"flag",1);
    }

    public ArrayList<Product>  report() {
        ArrayList<Product> products=new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM product WHERE flag=0";
        try {
            statement = dbConnection.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name_product");
                int quantity = rs.getInt("quantity");
                float price=rs.getFloat("price");
                int flag=rs.getInt("flag");
                products.add(new Product(name,quantity,price,flag));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(rs);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(dbConnection);
        return products;
    }

    public Product verifQuantity(String name,int q){
        ProductDao pd=new ProductDao();
        Product newP=null;
        ArrayList<Product> productArrayList=pd.report();
        for (Product p:productArrayList) {
            if(name.equals(p.getName_product()) && p.getFlag()==0){
                if(p.getQuantity()>=q)
                    newP=p;
            }
        }
        return newP;
    }

    public void update(Product p){
        delete(p,p.getName_product(), "name_product","quantity",p.getQuantity());
    }

    public void generatePdf(int nr) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("ProductReport"+nr+".pdf"));
        document.open();
        PdfPTable table = new PdfPTable(4);


        Font font = FontFactory.getFont(FontFactory.TIMES_BOLD, 25, BaseColor.BLACK);
        Paragraph p=new Paragraph("RAPORT PRODUSE",font);
        p.setSpacingAfter(20);
        p.setAlignment(1);
        addTableHeader(table);
        addRows(table);
        document.add(p);
        document.add(table);
        document.close();
    }

    private void addRows(PdfPTable table) {
        ProductDao dao = new ProductDao();
        int nr = 1;
        ArrayList<Product> productArrayList= dao.report();
        for (Product product : productArrayList) {
            Stream.of(Integer.toString(nr),product.getName_product(), Integer.toString(product.getQuantity()),Double.toString(product.getPrice())).forEach(row -> {
                PdfPCell cell = new PdfPCell();
                cell.setBackgroundColor(BaseColor.PINK);
                cell.setBorderWidth(3);
                cell.setBorderColor(BaseColor.DARK_GRAY);
                cell.setPhrase(new Phrase(row));
                table.addCell(cell);
            });
            nr++;
        }

    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("Nr.crt", "Produs", "Cantitate","Pret").forEach(columnTitle -> {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.PINK);
            header.setBorderWidth(3);
            header.setBorderColor(BaseColor.DARK_GRAY);
            header.setPhrase(new Phrase(columnTitle));
            table.addCell(header);
        });

    }

    public double getprice(String name){
        ArrayList<Product> products=new ArrayList<>();
        ProductDao dao=new ProductDao();
        products=dao.report();
        for (Product p:products) {
            if(p.getName_product().equals(name))
                return  p.getPrice();
        }
        return 0;
    }


}