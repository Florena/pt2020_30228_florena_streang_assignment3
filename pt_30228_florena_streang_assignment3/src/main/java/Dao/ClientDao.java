package Dao;

import ConnectionDB.ConnectionFactory;
import Model.Client;
import Model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.PrimitiveIterator;
import java.util.stream.Stream;

public class ClientDao extends GenericDao<Client> {

    public ClientDao() {
        super();
    }

    public void insertClient(Client p) {
        insert(p);
    }

    public ArrayList<Client> report() {
        ArrayList<Client> clients = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM client WHERE flag=0";
        try {
            statement = dbConnection.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name_client");
                String city = rs.getString("address");
                int flag = rs.getInt("flag");
                clients.add(new Client(name, city, flag));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(rs);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(dbConnection);
        return clients;
    }

    public void deleteClient(Client p, String pk, String column) {
        delete(p, pk, column, "flag", 1);
    }

    public void generatePdf(int nr) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("ClientReport"+nr+".pdf"));
        document.open();
        PdfPTable table = new PdfPTable(3);


        Font font = FontFactory.getFont(FontFactory.TIMES_BOLD, 25, BaseColor.BLACK);
        Paragraph p=new Paragraph("RAPORT CLIENTI",font);
        p.setSpacingAfter(20);
        p.setAlignment(1);
        addTableHeader(table);
        addRows(table);
        document.add(p);
        document.add(table);
        document.close();
    }

    private void addRows(PdfPTable table) {
        ClientDao dao = new ClientDao();
        int nr = 1;
        ArrayList<Client> clientArrayList = dao.report();
        for (Client client : clientArrayList) {
            Stream.of(Integer.toString(nr), client.getName_client(), client.getAddress()).forEach(columnTitle -> {
                PdfPCell cell = new PdfPCell();
                cell.setBackgroundColor(BaseColor.PINK);
                cell.setBorderWidth(3);
                cell.setBorderColor(BaseColor.DARK_GRAY);
                cell.setPhrase(new Phrase(columnTitle));
                table.addCell(cell);
            });
            nr++;
        }

    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("Nr.crt", "Nume si prenume", "Oras").forEach(columnTitle -> {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.PINK);
            header.setBorderWidth(3);
            header.setBorderColor(BaseColor.DARK_GRAY);
            header.setPhrase(new Phrase(columnTitle));
            table.addCell(header);
        });

    }
}