package Dao;

import ConnectionDB.ConnectionFactory;
import Model.OrderDetails;
import Model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Stream;

public class OrderDetailsDao extends GenericDao<OrderDetails> {
    public OrderDetailsDao() {
        super();
    }

    public int insertOrderDetails(OrderDetails order) {
        ProductDao q=new ProductDao();
        Product p=q.verifQuantity(order.getName_product(), order.getQuantity());
        if(p!=null){
            if(insert(order)==0) {
                p.update(order.getQuantity());
                q.update(p);
                return 0;

            }
        }
        else
            return -1;
        return -1;
    }

    public ArrayList<OrderDetails> report() {
        ArrayList<OrderDetails> orderDetails=new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM orderdetails where flag=0";
        try {
            statement = dbConnection.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                String name_client = rs.getString("name_client");
                String name_product=rs.getString("name_product");
                int quantity = rs.getInt("quantity");
                float price=rs.getFloat("price");
                int flag=rs.getInt("flag");
                orderDetails.add(new OrderDetails(name_client,name_product,quantity,price,0));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(rs);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(dbConnection);
        return orderDetails;
    }

    public void generatePdf(int nr) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("OrderReport"+nr+".pdf"));
        document.open();
        PdfPTable table = new PdfPTable(4);
        Font font = FontFactory.getFont(FontFactory.TIMES_BOLD, 25, BaseColor.BLACK);
        Paragraph p=new Paragraph("RAPORT COMENZI",font);
        p.setSpacingAfter(20);
        p.setAlignment(1);
        addTableHeader(table);
        addRows(table);
        document.add(p);
        document.add(table);
        document.close();
    }

    private void addRows(PdfPTable table) {
        OrderDetailsDao dao = new OrderDetailsDao();
        int nr = 1;
        ArrayList<OrderDetails> orderDetailsArrayList= dao.report();
        for (OrderDetails orderDetails : orderDetailsArrayList) {
            Stream.of(Integer.toString(nr),orderDetails.getName_client(),orderDetails.getName_product(), Integer.toString(orderDetails.getQuantity())).forEach(row -> {
                PdfPCell cell = new PdfPCell();
                cell.setBackgroundColor(BaseColor.PINK);
                cell.setBorderWidth(3);
                cell.setBorderColor(BaseColor.DARK_GRAY);
                cell.setPhrase(new Phrase(row));
                table.addCell(cell);
            });
            nr++;
        }

    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("Nr.crt", "Nume si prenume", "Produs","Cantitate").forEach(columnTitle -> {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.PINK);
            header.setBorderWidth(3);
            header.setBorderColor(BaseColor.DARK_GRAY);
            header.setPhrase(new Phrase(columnTitle));
            table.addCell(header);
        });

    }


}
