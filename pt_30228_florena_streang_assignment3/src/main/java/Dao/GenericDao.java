package Dao;

import ConnectionDB.ConnectionFactory;
import Model.Product;
import com.mysql.cj.xdevapi.ExprUnparser;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mysql.cj.xdevapi.ExprUnparser.quoteIdentifier;

public abstract class GenericDao<T> implements Reflection {
    protected final Logger LOGGER;

    {
        LOGGER = Logger.getLogger(GenericDao.class.getName());
    }

    private final Class<T> type;

    @SuppressWarnings("unused")
    protected GenericDao() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /***************Insert************************/

    public int insert(Object object) {

        Connection connection = null;
        PreparedStatement statement = null;
        int rs = 0;
        String query = createInsertStatementSql(object.getClass(), object.getClass().getSimpleName());
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            Field[] fields = object.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                field.setAccessible(true);
                Object value = field.get(object);
                statement.setObject((i + 1), value);
            }
            rs = statement.executeUpdate();

        } catch (SQLException | IllegalAccessException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:Insert " + e.getMessage());
            return -1;
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);

        }
        return 0;
    }

    private static String createInsertStatementSql(Class<?> clasa, String tableName) {
        StringBuilder fields = new StringBuilder("");
        StringBuilder vars = new StringBuilder();
        for (Field field : clasa.getDeclaredFields()) {
            if (fields.length() > 1) {
                fields.append(",");
                vars.append(",");
            }
            String name = field.getName();
            fields.append(quoteIdentifier(name));
            vars.append("?");
        }
        String sql = "INSERT INTO " + tableName + "(" + fields.toString() + ") VALUES (" + vars.toString() + ")";

        return sql;
    }


    /***********************************************************************/
    /**********************delete******************************************/

    public void delete(Object object, String primaryKey, String columnPk, String column, int val) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs;
        String query = createDeleteStatementSql(type, object.getClass().getSimpleName(), columnPk);
        try {
            connection = ConnectionFactory.getConnection();
            Class<?> zclass = object.getClass();
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            Field[] fields = zclass.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                field.setAccessible(true);
            }
            statement.setObject(1, primaryKey);
            rs = statement.executeQuery();
            while (rs.next()) {
                rs.updateInt(column, val);
                rs.updateRow();

            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:Insert" + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    private static String createDeleteStatementSql(Class<?> clasa, String tableName, String column) {
        StringBuilder sets = new StringBuilder("");
        String where = new String();
        String pair = quoteIdentifier(column) + "=?";
        String sql = "SELECT * FROM " + tableName + " WHERE " + pair + " && flag=0";

        return sql;
    }

    private static PreparedStatement createDeletePreparedStatement(Connection conn, Object object, String tableName, String primaryKey, String coloana) {
        PreparedStatement statement = null;
        try {
            Class<?> zclass = object.getClass();
            String Sql = createDeleteStatementSql(zclass, tableName, coloana);
            statement = conn.prepareStatement(Sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            Field[] fields = zclass.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                field.setAccessible(true);
            }
            statement.setObject(1, primaryKey);
        } catch (SecurityException | IllegalArgumentException
                | SQLException e) {
            String string = "Unable to create prepared statement: " + e.getMessage();
            throw new RuntimeException(string, e);
        }
        return statement;
    }
}
